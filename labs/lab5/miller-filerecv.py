# Izaak Miller
# Lab 5 - March 3rd, 2016
# Honorcode: The work submitted is my own unless cited otherwise

import socket

MAXFILES = 3 # receive at most this many files
MAXBLOCKS = 1000 # files can't exceed 1000 * 1024 bytes
SERVERTIMEOUT = 30 # after 30 seconds with no requests, shut down server
length = 0 # length of block

# Create a server socket that listens for files:
sock = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
sock.bind(('',12345))
sock.listen(1)
sock.settimeout(SERVERTIMEOUT)
try: # We handle a server socket timeout at the "except:" line

  for filenum in range(MAXFILES):

    connection,addr = sock.accept()
    print "Connected to" + str(addr)
    connection.settimeout(1)

    # Receive the "PUT" message, extract filename and filesize
    message = connection.recv(1024)
    put = message.split() # save the "PUT"
    filename = put[1] # save the filename
    filesize = int(put[2]) # save the filesize
    print "Received request " + str(message) # print request received

    connection.send("OK")
    # Create a new file:
    f = open('_'+filename,'wb')
    print "Saving "+filename+" in _"+filename

    try:

      # Loop to receive data and send back cumulative bytes received:
      while True:
          block = connection.recv(1024)
          length += len(block)  #increment length by the size of each block received
          connection.send(str(length) + ' Bytes')  #send the current number of bytes
          f.write(block)

          print "Received " + str(length) + " bytes"

          if length >= filesize: # If length larger than file size close client socket
              break

      print "Successfully received all " +  str(length) + " bytes of " + filename


      # No more data--close file, send final OK message, and close connection
      f.close()
      connection.send("OK Received " + str(length) + " bytes")
      connection.close()

    except socket.timeout: # This shouldn't happen!
        print "Connection timed out (this shouldn't happen!)"
        f.close()
        connection.close()

  sock.close() # Close after MAXFILES files have been received

except socket.timeout:
  print "Listener timed out"
  sock.close()
