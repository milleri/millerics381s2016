# Izaak Miller
# Lab 5 - March 3rd, 2016
# Honorcode: The work submitted is my own unless cited otherwise

from socket import *
import os.path

# Ask user for file name:
filename = raw_input("File to transfer: ")

try:
    # Open the file, get its size
    f = open(filename,'rb')
    filesize = os.path.getsize(filename)

    # Create socket and connect to the socket created by filerecv.py:
    sock = socket(AF_INET, SOCK_STREAM)
    sock.connect(('',12345))

    # Send the PUT message
    sock.send("PUT " + filename + " " + str(filesize))


    # Receive and print the "OK" message:
    ok = sock.recv(1024)
    print ok


    # Loop: send the file in blocks of size 1024; after each send,
    # receive a reply from the server and print it.
    while True:
        block = f.read(1024)

        # When we reach end of file, block is empty:
        if not block:
            print "File sent -- reached end of file"
            break

        # Sending block
        sock.send(block)
        n = sock.recv(1024)
        print n
    print "Closing file " + filename

    # Receive and print final "OK Received...." message, close
    # file, close socket
    closeMessage = sock.recv(1024)
    print closeMessage

    f.close()
    sock.close()

except socket.error:
    print "Socket error -- can't find server"

except IOError:
    print "No such file"
