# Izaak Miller
# Lab 3 February 18th 2016
# Honor code: The work submitted is my own unless cited otherwise

# We need the time package to calculate round-trip times:
import time

from socket import *

host = "localhost"
port = 12345
timeout = 1 # in seconds

# Create UDP client socket
# Note the use of SOCK_DGRAM for UDP datagram packet
clientsocket = socket(AF_INET, SOCK_DGRAM)

# Set socket timeout as 1 second
clientsocket.settimeout(timeout)

# Variables
k = 1
pingSent = 0
pingLost = 0
totalRTT = 0

# Ping 10 times:
for i in range(1,11):
    message = "Ping %d" %(k)
    k += 1
    #message = raw_input("What would you like to send? \n")
    try:
        print "Sending: " + message
	# Save current time (this is the start time):
        startTime = time.time()
	# Send the UDP packet containing the ping message
        clientsocket.sendto(message, (host, port))
	# Receive the server response
        d = clientsocket.recvfrom(1024)
        reply = d[0]
        addr = d[1]
	# Save current time (this is the end time)
        endTime = time.time()
	# Display the server response as an output
        print "Reply from ", addr, " is ", reply
    # print round trip time (difference between end time and start time):
        roundTime = endTime-startTime
        print "Response Time: ", roundTime
        totalRTT += roundTime
        pingSent += 1
    except:
        # Server does not respond; assume packet is lost and print message:
        print "Packet was lost."
        pingLost += 1
        continue

# Printing out successes and failures as well as averageRTT
print "\n%d Successes, %d Failures" %(pingSent, pingLost)
averageRTT = totalRTT/pingSent
print"Average RTT: ", averageRTT
# Close the client socket
clientsocket.close()

