# Izaak Miller
# lab 11
# Honor code: The work submitted is my own unless cited otherwise

import socket

# Create a "listening socket" that waits for requests:
listenSocket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
listenSocket.bind(('', 12345)) # runs on localhost
listenSocket.listen(1)

# It's up to you if you want to add a timeout for the listenSocket;
# it's okay to just let it run until you hit CTRL-C

while True:

  # New client (from Firefox or a terminal "telnet" command):
  tcpSocket,addr = listenSocket.accept()
  print "Sever connected to " + str(addr)
  print "Server received"

  # Client sends "GET http://hostname/filename HTTP/1.1" :
  getRequest = tcpSocket.recv(1024)
  print str(getRequest)

  # Extract website hostname and the name of the file (lab 9):
  parts = getRequest.partition('http://')
  temp = parts[2]
  temp1 = temp.split()
  temp2 = temp1[0].partition('/')
  host = temp2[0]
  File = temp2[1] + temp2[2]
  cachename = host + File

  print "Requested host: " + host
  print "Requested file: " + File

  cachename = cachename.replace("/", "-slash-")


  # Open a new socket to port 80 of the specied hostname:
  webSocket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
  webSocket.connect((host, 80))

  # Send a "GET" request for the file name (use HTTP/1.0 for this);
  # don't forget the two '\n\n' at the end of the request!
  # (The file "test.py" from lab 2 shows examples of these; however,
  # you can dispense with the "Localhost: header" when using HTTP/1.0.)

  try:
      cachefile = open(cachename,'rb') # Checking to see if we can open file
      cached = True
  except:
      cachefile = open(cachename,'wb') # No such file--create it for writing
      cached = False
  if cached:
      print "Sending cache file " + cachename + " to " + str(addr)

      while True:
        block = cachefile.read(1024) #read/write just like a regular flie
        if not block:
          print "File sent -- Reached end of file"
          break

        # Sending block
        tcpSocket.send(block)

      print "Closing file" + File + "\n"
      webSocket.close()
      tcpSocket.close()


  # Sending cached file to client
  else: # Creating cache file and saving to cache directory
    print "Creating cache file " + cachename
    print "Requesting file " + File
    webSocket.send('GET '+str(File)+' HTTP/1.0\n\n')

    # Receive the reply from the web server--it will be a (possibly
    # lengthy) file, so receive it as a file rather than multiple blocks

    cachefile = webSocket.makefile('rb')

    # Now you can read from this file just like any other file and send it
     # back over the tcpSocket; for instance, you can use a loop like
    # the one in "filesend.py" from lab 5
    print "Sending " + cachename + " to " + str(addr)
    while True:
      block = cachefile.read(1024) #read/write just like a regular flie
      if not block:
          print "File sent -- Reached end of file"
          break

      # Sending block
      tcpSocket.send(block)

    print "Closing file " + File + "\n"
    webSocket.close()
    tcpSocket.close()



  # close your webSocket and tcpSocket; leave open the listenSocket
