# Izaak Miller
# lab 10
# Honor code: The work submitted is my own unless cited otherwise

import socket

# Create a "listening socket" that waits for requests:
listenSocket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
listenSocket.bind(('', 12345)) # runs on localhost
listenSocket.listen(1)

# It's up to you if you want to add a timeout for the listenSocket;
# it's okay to just let it run until you hit CTRL-C

while True:

  # New client (from Firefox or a terminal "telnet" command):
  tcpSocket,addr = listenSocket.accept()

  # Client sends "GET http://hostname/filename HTTP/1.1" :
  getRequest = tcpSocket.recv(1024)

  # Extract website hostname and the name of the file (lab 9):
  parts = getRequest.partition('http://')
  temp = parts[2]
  temp1 = temp.split()
  temp2 = temp1[0].partition('/')
  host = temp2[0]
  File = temp2[1] + temp2[2]

  # Open a new socket to port 80 of the specied hostname:
  webSocket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
  webSocket.connect((host, 80))

  # Send a "GET" request for the file name (use HTTP/1.0 for this);
  # don't forget the two '\n\n' at the end of the request!
  # (The file "test.py" from lab 2 shows examples of these; however,
  # you can dispense with the "Localhost: header" when using HTTP/1.0.)

  webSocket.send('GET '+str(File)+' HTTP/1.0\n\n')

  # Receive the reply from the web server--it will be a (possibly
  # lengthy) file, so receive it as a file rather than multiple blocks

  remoteFile = webSocket.makefile('rb')

  # Now you can read from this file just like any other file and send it
  # back over the tcpSocket; for instance, you can use a loop like
  # the one in "filesend.py" from lab 5
  while True:
      block = remoteFile.read(1024) #read/write just like a regular flie
      if not block:
          print "File sent -- Reached end of file"
          break

      # Sending block
      tcpSocket.send(block)

  print "closing file" + File
  webSocket.close()
  tcpSocket.close()



  # close your webSocket and tcpSocket; leave open the listenSocket
