# Izaak Miller
# Lab 9
# Honor code: The work submitted is my own unless cited otherwise
import socket
listenSocket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
listenSocket.bind(('', 12345))
listenSocket.listen(1)

while True:
  tcpSocket,addr = listenSocket.accept()
  print "Server connected to " + str(addr)
  receivedMsg = tcpSocket.recv(1024)
  parts = receivedMsg.partition('http://')
  temp = parts[2]
  temp1 = temp.partition('/')
  host = temp1[0]
  slash = temp1[1]
  tempFile = temp1[2]
  temp2 = tempFile.split()
  File = temp2[0]
  if(File == "HTTP/1.1"):
    File = ''

  print "Host: " + str(host)
  print "File: " + str(slash) + str(File)
  print "received: " + receivedMsg
  tcpSocket.send('You requested ' + str(slash) + str(File) + ' from ' + str(host))
  tcpSocket.close()
